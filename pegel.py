import requests
import json
import sys
import socket

#Prefix for Graphite
prefix = "pegel"

#Graphite-Host
host = "localhost"

#Graphite-Port
port = 2003

#Try Requesting Data from API

try:
    #Limit to river
    #request = requests.get('https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json?includeTimeseries=true&waters=RHEIN&includeCurrentMeasurement=true')
    
    #All Pegel in Germany
    request = requests.get('https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json?includeTimeseries=true&includeCurrentMeasurement=true')
    
except requests.ConnectionError:
        print("Error when Downloading Data")
        exit()

#Convert JSON-Answer in JSON-Object
jrequest = json.loads(request.text)

#New Socket
sock = socket.socket()

#Connect to Graphite-Host
sock.connect((host, port))

#For each pegel entry
for pegel in jrequest:

    #Get River
    river = pegel["water"]["shortname"]
    
    #Replace Special Characters
    city = pegel["shortname"].replace(' ','').replace(u'ö', 'oe').replace(u'ü', 'ue').replace(u'ä', 'ae')#.decode('utf8', errors='replace')
    
    #Get Pegel
    currentPegel = int(pegel["timeseries"][0]["currentMeasurement"]["value"])
    
    #Generate Field Name
    field = prefix + ".pegel." + river + "." + city
    
    #Construct Message
    message = field + " " + str(currentPegel) + " " + str("-1") + "\n"
    message = message.encode('utf-8')
    
    #Send Message
    sock.send(message)

    #If there is Abfluss Data
    try:
        
        #Get Abfluss Value
        currentAbfluss = int(pegel["timeseries"][1]["currentMeasurement"]["value"])
        
        #Field Name
        field = prefix + ".abfluss." + river + "." + city
        
        #set Value
        value = currentAbfluss
        
        #Construct Message
        message = field + " " + str(currentAbfluss) + " " + str("-1") + "\n"
        message = message.encode('utf-8')
        
        #Send Message
        sock.send(message)

    except IndexError:
        continue

sock.close()
exit()