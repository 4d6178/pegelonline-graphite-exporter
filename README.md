# pegelonline-graphite-exporter

Laedt die Pegelstaende von der API der Seite "pegelonline.wsv.de" und fuegt sie in einer Graphite-Instanz ein. 

# Disclaimer

Die api ist unter Umstaenden nicht zur gewerblichen Nutzung freigegeben.
Bitte fuehren sie das Skript nicht zu oft aus, der Pegel aendert sich in der Regel alle 10-15 Minuten.

## Nutzung
Python Script evtl. anpassen und in Crontab einfuegen.